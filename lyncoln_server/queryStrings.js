const getAllAgentsString = () => 'SELECT * FROM LINCOLN.LINCOLN_AGENTI'
const updateAgentString = ({ Attivo, OraInizio, OraFine, id }) => `UPDATE LINCOLN.LINCOLN_AGENTI SET Attivo='${Attivo}', OraInizio='${OraInizio}', OraFine='${OraFine}' WHERE id=${id}`

module.exports = {
    getAllAgentsString,
    updateAgentString
}