const express = require("express")

const app = express()
const PORT = 4000 //later to be added in config or env
const agentsRoute = require("./routes/agents")
const cors = require("cors")
const path = require('path');
const dotenv = require("dotenv").config();


// app.use(cors({
//     origin: "*"
// }))
app.use(express.json())
app.use(express.static(path.join(__dirname, "/build")))

app.use("/agents", agentsRoute)

app.get("/lincoln", function (req, res) {
    res.sendFile(path.join(__dirname, "/build/index.html"), function (err) {
      if (err) {
        console.log("Reached ERROR!");
        res.status(500).send(err);
      }
    });
});

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`))
