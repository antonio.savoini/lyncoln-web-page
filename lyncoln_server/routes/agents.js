const express = require("express")
const router = express.Router()

const { getAllAgents, updateAgent } = require("../connections/DB")

router.get("/all", (req, res) => {
    console.log(`\nget all agents request\n`)
    try {
        getAllAgents(results => {
            res.status(200).json(results)
        })
    } catch {
        res.status(500)
    }
})


router.put("/update/:id", (req, res) => {
    console.log("update request received")
    const id = req.params.id
    const body = req.body

    const Attivo = body.Attivo
    const OraInizio = body.OraInizio
    const OraFine = body.OraFine

    try {
        // res.status(201).json(mockRows)
        updateAgent({ Attivo, OraInizio, OraFine, id }, result => {
            console.log("results from update agent: ", result)
            res.status(200)
        })
        
    } catch(err) {
        console.log("update request - in the fail. error: ", err)
        res.status(500)
    } finally {
        console.log("update request - in the final")
        res.end()
    }
})

/*
EXAMPLE PARAMS FOR UPDATE
{
    "id": 4,
    "OraInizio": "08:00:00",
    "OraFine": "19:00:00",
    "Attivo": 1
},

*/

module.exports = router