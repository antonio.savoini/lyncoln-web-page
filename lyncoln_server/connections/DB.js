const dotenv = require("dotenv").config();
const mysql = require("mysql")

const { getAllAgentsString,updateAgentString } = require("../queryStrings")

 const connection = mysql.createConnection({
     host: process.env.host,
     user: process.env.user,
     password: process.env.password,
     database: process.env.database
 })

console.log('coonnection',process.env);
/*const connection = mysql.createConnection({
    host: "localhost",
    user: "debian-sys-maint",
    password: "OS6S1ex9Rb0vEz7p",
    database: "lynctest"
})*/

connection.connect()
 
function getAllAgents(callback = (dbResults) => dbResults) {
    try{
    connection.query(getAllAgentsString(), (error, results, fields) => {
        if(error) console.log("error in get all agents function: ", error);
        callback(results)
    })
} catch(err){
 console.log('err getagents ',err)
}
}

function updateAgent({ Attivo, OraInizio, OraFine, id} ,callback = results => results) {
    try{
    connection.query(updateAgentString({Attivo, OraInizio, OraFine, id}),
    (error, results, fields) => {
        console.log("in update agent connection function")
        // if(error) throw error;
        if(error) console.log("error in update agents function: ", error)
        callback(results)
    })}
catch(err){
console.log('err uodate',err);
}
}

module.exports = {
    getAllAgents,
    updateAgent
}
