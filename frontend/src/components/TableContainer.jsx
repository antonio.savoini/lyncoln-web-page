import React from "react";
import TableHeader from "./TableHeader"

import RowContainer from "./Rows/RowContainer";

export default function TableContainer() {
    return <div className="TABLE_CONTAINER">
        <TableHeader />
        <RowContainer />
    </div>
}