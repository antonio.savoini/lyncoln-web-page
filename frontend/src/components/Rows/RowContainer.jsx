import React from 'react'
import Row from "./Row.jsx"

import Modal from "../Modal/index"

import ApiFactory from "../../endpoints"

export default function RowContainer() {
    const [ showModal, setShowModal ] = React.useState(false)
    const [ item, setItem ] = React.useState()

    const [ agents, setAgents ] = React.useState([])

    React.useEffect(() => {
        console.clear()
        getAgents()
    }, [])

    function handleModalClick(state, idObject) {
        setItem(agents.filter(arrayItem => arrayItem.id == idObject.id)[0])
        setShowModal(state)
    }

    async function getAgents() {
        let response = await fetch(ApiFactory.getAll())
        const data = await response.json()
        setAgents(data)
    }

    return (
        <>
        <div className="TABLE_ROW_CONTAINER">
            {
            agents.length > 0 && agents.map((item, index) => <Row key={item.id} name={item.NomeAgente}
                number={item.NumeroAgente} number2={item.NumeroAgente2}
                telType={item.TipoTelefono} code={item.CodiceAgente}
                startTime={item.OraInizio} finishTime={item.OraFine}
                active={item.Attivo} service={item.Servizio} counter={item.Contatore}
                isGrey={index%2!==0} agentID={item.id}
                clickHandler={(state, id) => handleModalClick(state, id)}
                 />)
            }
        </div>

        { showModal && <Modal item={item} handleCancel={()=>setShowModal(false)} /> }
    </>
    )
}
