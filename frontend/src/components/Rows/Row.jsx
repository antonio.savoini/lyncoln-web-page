import React from 'react'

export default function Row({ name, number, number2, telType, code,
    startTime, finishTime, active, service, counter, isGrey, agentID, clickHandler }) {

    function onClick(id={id: "", agentID: ""}) {
        if(id.id == id.agentID) {
            clickHandler(true, id)
        } else {
            console.log("not same id")
        }
    }
        
    return (
        <div className={`ROW ${isGrey && 'ROW_CHILD_GREY'}`} >

            <input value={name} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={number} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={number2} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={telType} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={code} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={startTime} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={finishTime} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={active} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={service} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

            <input value={counter} id={agentID}
            onClick={event => onClick({ id: event.target.id, agentID })}
            readOnly={true} />

        </div>
    )
}
