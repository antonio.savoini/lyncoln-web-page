import React from 'react'

const labels = [
    "Nome agente",
    "Numero agente",
    "Numero agente 2",
    "Tipo telefono",
    "Codice agente",
    "Ora inizio",
    "Ora fine",
    "Attivo",
    "Servizio",
    "Contatore"
]

export default function TableHeader() {
    return (
        <div className="TABLE_HEADER" >
            {
                labels.map(text => <h5 key={text}>{text}</h5>)
            }
        </div>
    )
}
