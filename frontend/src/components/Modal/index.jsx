import React, { useState } from 'react'

import styles from "./style.module.css"

import ApiFactory from "../../endpoints"

export default function Index({ item, handleCancel }) {

    const [ name, setName ] = useState(item.NomeAgente)

    const [ number, setNumber ] = useState(item.NumeroAgente)
    const [ number2, setNumber2 ] = useState(item.NumeroAgente2)

    const [ telType, setTelType ] = useState(item.TipoTelefono)

    const [ code, setCode ] = useState(item.CodiceAgente)
    
    const [ startTime, setStartTime ] = useState(item.OraInizio)

    const [ endTime, setEndTime ] = useState(item.OraFine)

    const [ active, setActive ] = useState(item.Attivo)

    const [ service, setService ] = useState(item.Servizio)

    const [ counter, setCounter ] = useState(item.Contatore)

    async function sendUpdatedAgent() {

        let paramObject = {
            OraInizio: startTime,
            OraFine: endTime, Attivo: active
        }

        console.log("paramObject: ", paramObject)

        try {
            let response = await fetch(ApiFactory.update(item.id), {
                method: "PUT",
                body: JSON.stringify(paramObject),
                mode: 'cors',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
                },
            })

            if(response.status === 200) {
                window.location.reload()
            }
        } catch(err) {
            console.log("Error in update agent: ", err)
        }

        
    }
    
    return (
        <div className={styles['MODAL_COVER']} >
            <div className={styles['MODAL']} >

                <span>
                    <div>
                        <label>Nome</label>
                        <br/>
                        <input value={name} id={item.id}
                        onChange={event => setName(event.target.value)}
                        readOnly={true}
                        />
                    </div>

                    <div>
                        <label>Numero Agente</label>
                        <br/>
                        <input value={number} id={item.id} readOnly={true}
                        onChange={event => setNumber(event.target.value)}/>
                    </div>

                    <div>
                        <label>Numero Agente 2</label>
                        <br/>
                        <input value={number2} id={item.id} readOnly={true}
                        onChange={event => setNumber2(event.target.value)}/>
                    </div>

                    <div>
                        <label>Tipo Telefono</label>
                        <br/>
                        <input value={telType} id={item.id} readOnly={true}
                        onChange={event => setTelType(event.target.value)}/>
                    </div>
                </span>

                <span>
                    <div>
                        <label>Codice Agente</label>
                        <br/>
                        <input value={code} id={item.id} readOnly={true}
                        onChange={event => setCode(event.target.value)}/>
                    </div>

                    <div>
                        <label>Ora Inizio</label>
                        <br/>
                        <input value={startTime} id={item.id} type="time"
                        onChange={event => setStartTime(event.target.value)}/>
                    </div>

                    <div>
                        <label>Ora Fine</label>
                        <br/>
                        <input value={endTime} id={item.id} type="time"
                        onChange={event => setEndTime(event.target.value)}/>
                    </div>

                    <div>
                        <label>Attivo</label>
                        <br/>
                        {/* <input value={active} id={item.id}
                        onChange={event => setActive(event.target.value)}/> */}
                        <select value={active} onChange={event => setActive(event.target.value)} >
                            <option value="1" >1</option>
                            <option value="0" >0</option>
                        </select>
                    </div>
                </span>

                <span>
                    <div>
                        <label>Servizio</label>
                        <br/>
                        <input value={service} id={item.id} readOnly={true}
                        onChange={event => setService(event.target.value)} />
                    </div>

                    <div>
                        <label>Contatore</label>
                        <br/>
                        <input value={counter} id={item.id} readOnly={true}
                        onChange={event => setCounter(event.target.value)} />
                    </div>
                </span>

                <div className={styles['MODAL_FOOTER']} >
                        <button className={styles['MODAL_UPDATE_BTN']}
                        onClick={() => sendUpdatedAgent()}>Aggiorna</button>

                        <button className={styles['MODAL_CANCEL_BTN']}
                        onClick={handleCancel}>Annulla</button>
                </div>

            </div>
        </div>
    )
}
