const proxy = require('http-proxy-middleware');
/**
 * middleware proxy to connect server.
 * it channels all the API calls from frontend to server.
 * if you change server port remember to update in this file.
 * @param  {object} app
 */
module.exports = function (app) {
  app.use(
    proxy(['/agents'], { target: 'http://localhost:4000' }) //https://api.sinerpay.com/http://localhost:4000
  );
};

