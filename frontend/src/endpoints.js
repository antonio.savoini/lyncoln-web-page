const baseURL = "http://localhost:4000/"

const endpoints = {
    getAll: () => `/agents/all`,
    update: (id) => `/agents/update/${id}`,
}

export default endpoints
